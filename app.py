from flask import Flask, render_template, request

from utils import get_contact_data_from_json, filter_contacts

app = Flask(__name__)


@app.route("/")
def index():
    """
    Main index view. Returns all json data.
    """
    contacts_list = get_contact_data_from_json('exercise-data.json')
    return render_template('base.html', contacts_list=contacts_list)


@app.route('/search')
def search():
    """
    Search view. Take param from request and return only contacts with param in some fiels.
    """
    query = request.args.get('searched_text')
    contacts_list = filter_contacts(query, 'exercise-data.json')
    return render_template('base.html', contacts_list=contacts_list)


if __name__ == '__main__':
    app.run(debug=True)
