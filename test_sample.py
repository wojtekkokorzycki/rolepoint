import pytest as pytest

from utils import get_contact_data_from_json, filter_contacts


def test_is_json_file_exists():
    import os.path
    assert os.path.isfile('exercise-data.json')


all_data_json = [
    {'job_history': ['Sibelius'], 'company': 'Mattis Corporation', 'email': 'vehicula@et.com', 'city': 'Westerlo', 'name': 'David Harrington', 'country': 'Panama'},
    {'job_history': ['Chami'], 'company': 'Vitae Erat LLC', 'email': 'non.arcu@dui.com', 'city': 'Osgoode', 'name': 'Madeson Hopkins', 'country': 'Tokelau'},
    {'job_history': [''], 'company': 'Lacus Cras Associates', 'email': 'tempus.non.lacinia@ultricesposuerecubilia.com', 'city': 'Gelbressee', 'name': 'Nero Acosta', 'country': 'Panama'}
]


def test_get_all():
    assert get_contact_data_from_json('test-data.json') == all_data_json


params = [
    ('Sibelius', [{'job_history': ['Sibelius'], 'company': 'Mattis Corporation', 'email': 'vehicula@et.com', 'city': 'Westerlo', 'name': 'David Harrington', 'country': 'Panama'}]),
    ('Vitae', [{'job_history': ['Chami'], 'company': 'Vitae Erat LLC', 'email': 'non.arcu@dui.com', 'city': 'Osgoode', 'name': 'Madeson Hopkins', 'country': 'Tokelau'}]),
    ('Panama', [
        {'job_history': ['Sibelius'], 'company': 'Mattis Corporation', 'email': 'vehicula@et.com', 'city': 'Westerlo', 'name': 'David Harrington', 'country': 'Panama'},
        {'job_history': [''], 'company': 'Lacus Cras Associates', 'email': 'tempus.non.lacinia@ultricesposuerecubilia.com', 'city': 'Gelbressee', 'name': 'Nero Acosta', 'country': 'Panama'}
    ]),
]


@pytest.mark.parametrize("test_input, expected", params)
def test_query_param(test_input, expected):
    print(test_input)
    print(filter_contacts(test_input, 'test-data.json'))
    print(expected)
    assert filter_contacts(test_input, 'test-data.json') == expected
