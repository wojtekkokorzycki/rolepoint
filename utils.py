"""Utils for getting contact data from json file"""
import json


# TODO: better storage way or pagination
def get_contact_data_from_json(file_path):
    """Get all data from file"""
    with open(file_path) as json_file:
        json_data = json.load(json_file)
    return json_data


# TODO: Maybe database to query data faster?
def filter_contacts(query, file_path):
    all_contacts = get_contact_data_from_json(file_path)
    result = []
    for contact in all_contacts:
        for field in contact:
            if query in contact[field]:
                result.append(contact)
                break
    return result
