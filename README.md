# Simple flask based search using json file as data storage

## Clone repo
git clone https://wojtekkokorzycki@bitbucket.org/wojtekkokorzycki/rolepoint.git

cd role_point
## Make and activate virtualenv
virtualenv -p python3 venv

. venv/bin/activate


## Install requirements
pip install -r requirements.txt

## Run app
python app.py

## Run tests
pytest

